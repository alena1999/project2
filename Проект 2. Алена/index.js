const express = require('express')
const app = express();
app.use(express.json());
app.use(express.urlencoded());

const bodyParser = require('body-parser')
app.use(bodyParser.urlencoded({extended: true}));

var pool = require('./config.js')
app.set('view engine', 'ejs')
app.get('/', (req, res) => res.render('user'));
app.get('/manager', (req, res) => res.render('manager'));

app.listen(8080, () => console.log('Сервер запущен'));

//Переход на добавленеи маршрутов
app.post('/usertoadmin', (req, res) => {
    res.render('admin.ejs', function(err, html) { res.send(html); });
});

//Переход на покупку билета
app.post('/return_to_buy_page', (req, res) => {
    res.render('user.ejs', function(err, html) { res.send(html); });
});

//Добавление нового маршрута
app.post('/add_new_route', (req, res) => {

    let route_out = req.body.route_out
    let route_to = req.body.route_to
    let departure_time = req.body.departure_time
    let ticket_price = req.body.ticket_price
    let route_number = req.body.route_number
     
    pool.getConnection((err, con) => 
    {
        con.query(`insert into Route (route_out, route_to, departure_time, ticket_price, route_number) values (?,?,?,?,?)`, [route_out, route_to, departure_time, ticket_price, route_number], (error) => 
            {
                if (error) throw error

                res.render('admin.ejs', function(err, html) 
                { 
                    res.send(html); 
                });
            })
        console.log(`Добавлен новый маршрут`);
    })
});

//Удаление маршрута
app.post('/delete_route', (req, res) => {

    let delete_route_number = req.body.delete_route_number

    pool.getConnection((err, con) => 
    {
        con.query(`delete from Route where route_number = (?)`, [delete_route_number], (error) => 
        {
            if (error) throw error

            res.render('admin.ejs', function(err, html) 
            { 
                res.send(html); 
            });
        })
        
        con.query(`delete from Clients where Num_route = (?)`, [delete_route_number], (error) => {
            if (error) throw error

            res.render('admin.ejs', function(err, html) { res.send(html); });
        })
        con.release();
        
        console.log(`Удаление маршрута`);
    })
});



//Показать все маршруты
app.get('/show_routes', (req,res) => {   

    pool.getConnection((err, con) => {

        con.query(`select * from Route`, [], (error, rows) => {

            if (error) throw error
    
            for (var n in rows)                
                {                     
                    var route_out = rows[n].route_out;
                    var route_to = rows[n].route_to;
                    var departure_time = rows[n].departure_time;                    
                    var ticket_price = rows[n].ticket_price;
                    var route_number = rows[n].route_number;
                }

            res.render('admin.ejs', {rows: rows});
        })
    })
});

//Показать маршруты по определенному городу
app.post('/show_routes_out_or_to', (req,res) => { 
    
    let valueselect = req.body.valueselect
    let city_name = req.body.city_name

    console.log(`${valueselect} ${city_name}`)

    pool.getConnection((err, con) => {

        if(valueselect == "out")
        {
            con.query(`select * from Route where route_out = (?)`, [city_name], (error, rows) => {

                if (error) throw error
    
                for (var n in rows)                
                    {                     
                        var route_out = rows[n].route_out;
                        var route_to = rows[n].route_to;
                        var departure_time = rows[n].departure_time;                    
                        var ticket_price = rows[n].ticket_price;
                        var route_number = rows[n].route_number;
                    }

                res.render('admin.ejs', {rows: rows});
            })
        }

        if(valueselect == "to")
        {
            con.query(`select * from Route where route_to = (?)`, [city_name], (error, rows) => {

                if (error) throw error
    
                for (var n in rows)                
                    {                     
                        var route_out = rows[n].route_out;
                        var route_to = rows[n].route_to;
                        var departure_time = rows[n].departure_time;                    
                        var ticket_price = rows[n].ticket_price;
                        var route_number = rows[n].route_number;
                    }

                res.render('admin.ejs', {rows: rows});
            })
        }
    })
});

//Показать пассажиров для определенного рейса 
app.post('/show_clients', (req,res) => { 
    
    let num_route = req.body.num_route

    console.log(`${num_route}`)

    pool.getConnection((err, con) => {

        con.query(`select * from Clients where Num_route = (?)`, [num_route], (error, rows) => {

            if (error) throw error
    
            for (var n in rows)                
                {                
                    var Num_pass = rows[n].Num_pass;
                    var Name_pass = rows[n].Name_pass;
                    var Surname = rows[n].Surname;                    
                    var Lastname = rows[n].Lastname;
                    var Birthday = rows[n].Birthday;
                    var Num_set = rows[n].Num_set;
                    console.log(`${Num_pass} ${Name_pass} ${Surname} ${Lastname} ${Birthday} ${Num_set}`)
                }
            res.render('admin.ejs', {rows: rows});
        })        
    })
});

//Показать маршруты для пассажиров 
app.post('/show_routes_for_pass', (req,res) => { 
    
    let from_city = req.body.from_city
    let to_city = req.body.to_city
    let departure_time_pass = req.body.departure_time_pass

    pool.getConnection((err, con) => {

        con.query(`select * from Route where route_out = (?) and route_to = (?) and departure_time = (?)`, [from_city, to_city, departure_time_pass], (error, rows) => {

            if (error) throw error
    
            for (var n in rows)                
                {                
                    var route_out = rows[n].route_out;
                    var route_to = rows[n].route_to;
                    var departure_time = rows[n].departure_time;   
                    var ticket_price = rows[n].ticket_price;                 
                    var route_number = rows[n].route_number;
                }
            res.render('user.ejs', {rows: rows});
        })        
    })
});

//Покупка билета
app.post('/buy_ticket', (req, res) => {

    let num_pass = req.body.num_pass
    let name_pass = req.body.name_pass
    let surname = req.body.surname
    let lastname = req.body.lastname
    let departure_time = req.body.departure_time
    let num_route = req.body.num_route
    let num_set = req.body.num_set
     
    pool.getConnection((err, con) => 
    {
        con.query(`insert into Clients (Num_pass, Name_pass, Surname, Lastname, Birthday, Num_route, Num_set) values (?,?,?,?,?,?,?)`, [num_pass, name_pass, surname, lastname, departure_time, num_route, num_set], (error) => 
            {
                if (error) throw error

                res.render('user.ejs', function(err, html) 
                { 
                    res.send(html); 
                });
            })
        console.log(`Билет куплен`);
    })
});